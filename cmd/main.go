package main

import (
	"catalog/config"
	"catalog/gprc"
	"catalog/gprc/client"
	"catalog/pkg/logger"
	"catalog/storage/postres"
	"context"
	"fmt"
	"net"

	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	pgStore, err := postres.New(context.Background(), cfg, log)
	if err != nil {
		fmt.Println("stoe", pgStore)
		log.Error("error is while connecting to db", logger.Error(err))
	}

	services, err := client.NewGrpcClient(cfg, log)
	if err != nil {
		log.Error("error is while initializing grpc clients", logger.Error(err))
		return
	}

	grpcServer := gprc.SetUpServer(pgStore, services, log)

	lis, err := net.Listen("tcp", cfg.ServiceGrpcHost+cfg.ServiceGrpcPort)
	if err != nil {
		log.Error("error is while listening service", logger.Error(err))
		return
	}

	log.Info("Server is running...", logger.Any("gprc port", cfg.ServiceGrpcPort))
	if err := grpcServer.Serve(lis); err != nil {
		log.Error("error is while serving", logger.Error(err))
	}
}
