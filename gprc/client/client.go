package client

import (
	"catalog/config"
	pbC "catalog/genproto/catalog_service"
	"catalog/pkg/logger"

	"google.golang.org/grpc"
)

type IServiceManager interface {
	CategoryService() pbC.CategoryServiceClient
	ProductService() pbC.ProductServiceClient
}

type grpcClient struct {
	categoryService pbC.CategoryServiceClient
	productService  pbC.ProductServiceClient
}

func NewGrpcClient(cfg config.Config, log logger.ILogger) (IServiceManager, error) {
	connUserService, err := grpc.Dial(cfg.ServiceGrpcHost+cfg.ServiceGrpcPort, grpc.WithInsecure())
	if err != nil {
		log.Error("error is while dial grpc", logger.Error(err))
		return &grpcClient{}, err
	}
	return &grpcClient{
		categoryService: pbC.NewCategoryServiceClient(connUserService),
		productService:  pbC.NewProductServiceClient(connUserService),
	}, nil
}

func (g *grpcClient) CategoryService() pbC.CategoryServiceClient {
	return g.categoryService
}

func (g *grpcClient) ProductService() pbC.ProductServiceClient {
	return g.productService
}
