package gprc

import (
	pbC "catalog/genproto/catalog_service"
	"catalog/gprc/client"
	"catalog/pkg/logger"
	"catalog/service"
	"catalog/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *grpc.Server {
	grpcServer := grpc.NewServer()

	pbC.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(strg, services, log))
	pbC.RegisterProductServiceServer(grpcServer, service.NewProductService(strg, services, log))

	reflection.Register(grpcServer)

	return grpcServer
}
