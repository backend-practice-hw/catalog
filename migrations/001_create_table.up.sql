CREATE TYPE "product_type_enum" AS ENUM (
    'modifier',
    'product'
);


CREATE TABLE "categories" (
                              "id" uuid PRIMARY KEY NOT NULL,
                              "title" text,
                              "image" text,
                              "active" bool,
                              "parent_id" uuid references categories(id),
                              "order_number" serial NOT NULL,
                              "created_at" timestamp default 'Now()',
                              "updated_at" timestamp default 'Now()',
                              "deleted_at" integer default 0
);

CREATE TABLE "products" (
                            "id" uuid PRIMARY KEY NOT NULL,
                            "title" text,
                            "description" text,
                            "photos" text,
                            "order_number" serial NOT NULL,
                            "active" bool NOT NULL,
                            "type" product_type_enum,
                            "price" float,
                            "category_id" uuid,
                            "created_at" timestamp default 'Now()',
                            "updated_at" timestamp default 'Now()',
                            "deleted_at" integer default 0
);

ALTER TABLE "categories" ADD FOREIGN KEY ("parent_id") REFERENCES "categories" ("id");

ALTER TABLE "products" ADD FOREIGN KEY ("category_id") REFERENCES "categories" ("id");
