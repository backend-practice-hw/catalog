package service

import (
	pbC "catalog/genproto/catalog_service"
	"catalog/gprc/client"
	"catalog/pkg/logger"
	"catalog/storage"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
)

type categoryService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbC.UnimplementedCategoryServiceServer
}

func NewCategoryService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *categoryService {
	return &categoryService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (c *categoryService) Create(ctx context.Context, request *pbC.CreateCategory) (*pbC.Category, error) {
	response, err := c.storage.Category().Create(ctx, request)
	if err != nil {
		c.log.Error("error in service layer while creating category", logger.Error(err))
		return nil, err
	}
	return response, nil
}

func (c *categoryService) Get(ctx context.Context, key *pbC.PrimaryKey) (*pbC.Category, error) {
	response, err := c.storage.Category().Get(ctx, key)
	if err != nil {
		c.log.Error("error in service layer while getting category by id", logger.Error(err))
		return nil, err
	}
	return response, nil
}

func (c *categoryService) GetList(ctx context.Context, request *pbC.CategoryRequest) (*pbC.CategoryResponse, error) {
	categories, err := c.storage.Category().GetList(ctx, request)
	if err != nil {
		c.log.Error("error in service layer while get category list", logger.Error(err))
		return nil, err
	}

	return categories, nil
}

func (c *categoryService) Update(ctx context.Context, category *pbC.Category) (*pbC.Category, error) {
	response, err := c.storage.Category().Update(ctx, category)
	if err != nil {
		c.log.Error("error in service layer while updating category", logger.Error(err))
		return nil, err
	}

	return response, nil
}

func (c *categoryService) Delete(ctx context.Context, key *pbC.PrimaryKey) (*empty.Empty, error) {
	_, err := c.storage.Category().Delete(ctx, key)
	return nil, err
}
