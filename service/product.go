package service

import (
	pbC "catalog/genproto/catalog_service"
	"catalog/gprc/client"
	"catalog/pkg/logger"
	"catalog/storage"
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
)

type productService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pbC.UnimplementedProductServiceServer
}

func NewProductService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *productService {
	return &productService{
		storage:  storage,
		services: services,
		log:      log,
	}
}

func (c *productService) Create(ctx context.Context, request *pbC.CreateProduct) (*pbC.Product, error) {
	response, err := c.storage.Product().Create(ctx, request)
	if err != nil {
		c.log.Error("error in service layer while creating product", logger.Error(err))
		return nil, err
	}
	return response, nil
}

func (c *productService) Get(ctx context.Context, key *pbC.PrimaryKey) (*pbC.Product, error) {
	response, err := c.storage.Product().Get(ctx, key)
	if err != nil {
		c.log.Error("error in service layer while getting product by id", logger.Error(err))
		return nil, err
	}
	return response, nil
}

func (c *productService) GetList(ctx context.Context, request *pbC.GetProductRequest) (*pbC.GetProductResponse, error) {
	products, err := c.storage.Product().GetList(ctx, request)
	fmt.Println("req", request)
	if err != nil {
		fmt.Println("produc", products)
		c.log.Error("error in service layer while get product list", logger.Error(err))
		return nil, err
	}

	return products, nil
}

func (c *productService) Update(ctx context.Context, request *pbC.Product) (*pbC.Product, error) {
	response, err := c.storage.Product().Update(ctx, request)
	if err != nil {
		c.log.Error("error in service layer while updating product", logger.Error(err))
		return nil, err
	}

	return response, nil
}

func (c *productService) Delete(ctx context.Context, key *pbC.PrimaryKey) (*empty.Empty, error) {
	_, err := c.storage.Product().Delete(ctx, key)
	return nil, err
}
