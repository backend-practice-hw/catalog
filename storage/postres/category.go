package postres

import (
	pbC "catalog/genproto/catalog_service"
	"catalog/pkg/logger"
	"catalog/storage"
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type categoryRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCategoryRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICategoryStorage {
	return &categoryRepo{
		db:  db,
		log: log,
	}
}

func (c *categoryRepo) Create(ctx context.Context, request *pbC.CreateCategory) (*pbC.Category, error) {
	category := pbC.Category{}
	query := `insert into categories (id, title, image, active, parent_id)
                          values ($1, $2, $3, $4, $5)
                          returning id, title,image, active, parent_id, order_number, created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetTitle(),
		request.GetImage(),
		request.GetActive(),
		request.GetParentId(),
	).Scan(
		&category.Id,
		&category.Title,
		&category.Image,
		&category.Active,
		&category.ParentId,
		&category.OrderNumber,
		&category.CreatedAt,
		&category.UpdatedAt,
	); err != nil {
		c.log.Error("error is while creating category", logger.Error(err))
		return nil, err
	}
	return &category, nil
}
func (c *categoryRepo) Get(ctx context.Context, key *pbC.PrimaryKey) (*pbC.Category, error) {
	category := pbC.Category{}
	query := `select id, title, active, parent_id, order_number, created_at::text, updated_at::text from categories where id = $1 and deleted_at = 0 `
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&category.Id,
		&category.Title,
		&category.Active,
		&category.ParentId,
		&category.OrderNumber,
		&category.CreatedAt,
		&category.UpdatedAt,
	); err != nil {
		c.log.Error("error is while getting category by id", logger.Error(err))
		return nil, err
	}

	return &category, nil
}

func (c *categoryRepo) GetList(ctx context.Context, request *pbC.CategoryRequest) (*pbC.CategoryResponse, error) {
	var (
		page                      = request.Page
		offset                    = (page - 1) * request.Limit
		filter, query, countQuery string
		count                     = 0
		categories                []*pbC.Category
		pagination                string
	)

	pagination = ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetTitle() != "" {
		filter += fmt.Sprintf(` and title ilike '%s' `, request.GetTitle())
	}

	countQuery = `select count(1) from categories where deleted_at = 0 ` + filter
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while scanning count", logger.Error(err))
		return &pbC.CategoryResponse{}, err
	}

	query = `select id, title, active, parent_id, order_number, created_at::text, updated_at::text from categories  where deleted_at = 0 ` + filter + pagination
	rows, err := c.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		c.log.Error("error is while selecting all from room categories", logger.Error(err))
		return &pbC.CategoryResponse{}, err
	}

	for rows.Next() {
		category := pbC.Category{}
		if err := rows.Scan(
			&category.Id,
			&category.Title,
			&category.Active,
			&category.ParentId,
			&category.OrderNumber,
			&category.CreatedAt,
			&category.UpdatedAt,
		); err != nil {
			c.log.Error("error is while scanning all from category", logger.Error(err))
			return &pbC.CategoryResponse{}, err
		}
		categories = append(categories, &category)
	}
	return &pbC.CategoryResponse{
		Categories: categories,
		Count:      int32(count),
	}, nil
}

func (c *categoryRepo) Update(ctx context.Context, category *pbC.Category) (*pbC.Category, error) {
	updatedCategory := pbC.Category{}
	query := `update categories set title = $1, active = $2, parent_id = $3, updated_at = now() 
                  where id = $4 and deleted_at = 0 returning id, title, active, parent_id, created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query, category.GetTitle(), category.GetActive(), category.GetParentId(), category.GetId()).
		Scan(
			&updatedCategory.Id,
			&updatedCategory.Title,
			&updatedCategory.Active,
			&updatedCategory.ParentId,
			&updatedCategory.CreatedAt,
			&updatedCategory.UpdatedAt,
		); err != nil {
		c.log.Error("error is while updating category", logger.Error(err))
		return nil, err
	}
	return &updatedCategory, nil
}

func (c *categoryRepo) Delete(ctx context.Context, key *pbC.PrimaryKey) (*empty.Empty, error) {
	query := `update categories set deleted_at = extract(epoch from current_timestamp) where id = $1 `
	rowsAffected, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		c.log.Error("error is while deleting category", logger.Error(err))
		return nil, err
	}
	if row := rowsAffected.RowsAffected(); err != nil {
		c.log.Error("rows affected", logger.Any("rows", row))
	}
	return nil, err
}
