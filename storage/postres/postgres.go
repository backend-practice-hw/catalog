package postres

import (
	"catalog/config"
	"catalog/pkg/logger"
	"catalog/storage"
	"context"

	"fmt"
	"strings"

	"github.com/golang-migrate/migrate/v4"
	"github.com/jackc/pgx/v5/pgxpool"

	_ "github.com/golang-migrate/migrate/v4/database"          //database is needed for migration
	_ "github.com/golang-migrate/migrate/v4/database/postgres" //postgres is used for database
	_ "github.com/golang-migrate/migrate/v4/source/file"       //file is needed for migration url
	_ "github.com/lib/pq"
)

type Store struct {
	db  *pgxpool.Pool
	log logger.ILogger
	cfg config.Config
}

func New(ctx context.Context, cfg config.Config, log logger.ILogger) (storage.IStorage, error) {
	url := fmt.Sprintf(
		`postgres://%s:%s@%s:%s/%s?sslmode=disable`,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDB,
	)

	fmt.Println("url", url)
	poolConfig, err := pgxpool.ParseConfig(url)
	if err != nil {
		return nil, err
	}

	poolConfig.MaxConns = 100

	pool, err := pgxpool.NewWithConfig(ctx, poolConfig)
	if err != nil {
		return nil, err
	}

	//migration
	m, err := migrate.New("file://migrations", url)
	if err != nil {
		return nil, err
	}

	if err = m.Up(); err != nil {
		if !strings.Contains(err.Error(), "no change") {
			fmt.Println("entered", err)
			version, dirty, err := m.Version()
			if err != nil {
				return nil, err
			}

			if dirty {
				version--
				if err = m.Force(int(version)); err != nil {
					return nil, err
				}
			}
			return nil, err
		}
	}

	return Store{
		db:  pool,
		cfg: cfg,
		log: log,
	}, nil
}

func (s Store) Close() {
	s.db.Close()
}

func (s Store) Category() storage.ICategoryStorage {
	return NewCategoryRepo(s.db, s.log)
}

func (s Store) Product() storage.IProductStorage {
	return NewProductRepo(s.db, s.log)
}
