package postres

import (
	pbC "catalog/genproto/catalog_service"
	"catalog/pkg/logger"
	"catalog/storage"
	"context"
	"fmt"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
)

type productRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewProductRepo(db *pgxpool.Pool, log logger.ILogger) storage.IProductStorage {
	return &productRepo{
		db:  db,
		log: log,
	}
}

func (c *productRepo) Create(ctx context.Context, request *pbC.CreateProduct) (*pbC.Product, error) {
	product := pbC.Product{}
	query := `insert into products (id, title, description, photos, active, type, price, category_id)
                          values ($1, $2, $3, $4, $5, $6, $7, $8)
    returning id, title, description, photos, active, type, price, category_id, order_number, created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.GetTitle(),
		request.GetDescription(),
		request.GetPhotos(),
		request.GetActive(),
		request.GetType(),
		request.GetPrice(),
		request.GetCategoryId(),
	).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photos,
		&product.Active,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.OrderNumber,
		&product.CreatedAt,
		&product.UpdatedAt,
	); err != nil {
		c.log.Error("error is while creating product", logger.Error(err))
		return nil, err
	}
	return &product, nil
}
func (c *productRepo) Get(ctx context.Context, key *pbC.PrimaryKey) (*pbC.Product, error) {
	product := pbC.Product{}
	query := `select id, title, description, photos, active, type, price, category_id, order_number,
                          created_at::text, updated_at::text from products where id = $1 and deleted_at = 0 `
	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photos,
		&product.Active,
		&product.Type,
		&product.Price,
		&product.CategoryId,
		&product.OrderNumber,
		&product.CreatedAt,
		&product.UpdatedAt,
	); err != nil {
		c.log.Error("error is while getting product by id", logger.Error(err))
		return nil, err
	}

	return &product, nil
}
func (c *productRepo) GetList(ctx context.Context, request *pbC.GetProductRequest) (*pbC.GetProductResponse, error) {
	var (
		offset                    = (request.GetPage() - 1) * request.GetLimit()
		filter, query, countQuery string
		count                     = 0
		products                  []*pbC.Product
		pagination                string
	)

	pagination = ` ORDER BY created_at desc LIMIT $1 OFFSET $2`

	if request.GetTitle() != "" {
		filter += fmt.Sprintf(` and title ilike '%s' `, request.GetTitle())
	}
	if request.GetType() != "" {
		filter += fmt.Sprintf(` and type = '%s' `, request.GetType())
	}
	if request.GetActive() != true && request.GetActive() != false {
		filter += ``
	}
	if request.GetActive() == false {
		filter += fmt.Sprintf(` and active = %v `, request.GetActive())
	}
	if request.GetActive() == true {
		filter += fmt.Sprintf(` and active = %v `, request.GetActive())
	}
	if request.GetCategoryId() != "" {
		filter += fmt.Sprintf(` and category_id = '%s' `, request.GetCategoryId())
	}

	countQuery = `select count(1) from products where deleted_at = 0 ` + filter
	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error is while scanning count", logger.Error(err))
		return &pbC.GetProductResponse{}, err
	}

	query = `select id, title, description, photos, active, type, price, category_id, order_number,
                          created_at::text, updated_at::text from products where  deleted_at = 0 ` + filter + pagination
	rows, err := c.db.Query(ctx, query, request.GetLimit(), offset)
	if err != nil {
		c.log.Error("error is while selecting all from room categories", logger.Error(err))
		return &pbC.GetProductResponse{}, err
	}

	for rows.Next() {
		product := pbC.Product{}
		if err := rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photos,
			&product.Active,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.OrderNumber,
			&product.CreatedAt,
			&product.UpdatedAt,
		); err != nil {
			c.log.Error("error is while scanning all from category", logger.Error(err))
			return &pbC.GetProductResponse{}, err
		}
		products = append(products, &product)
	}

	fmt.Println("proudct", products)
	return &pbC.GetProductResponse{
		Products: products,
		Count:    int32(count),
	}, nil
}
func (c *productRepo) Update(ctx context.Context, request *pbC.Product) (*pbC.Product, error) {
	product := pbC.Product{}
	query := `update products set title = $1, description = $2, photos = $3, active = $4, type = $5, price = $6, category_id = $7, updated_at = now() 
                  where id = $8 and deleted_at = 0 returning id, title, description, photos, active, type, price, category_id, order_number,
                          created_at::text, updated_at::text`
	if err := c.db.QueryRow(ctx, query,
		request.GetTitle(),
		request.GetDescription(),
		request.GetPhotos(),
		request.GetActive(),
		request.GetType(),
		request.GetPrice(),
		request.GetCategoryId(),
		request.GetId(),
	).
		Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photos,
			&product.Active,
			&product.Type,
			&product.Price,
			&product.CategoryId,
			&product.OrderNumber,
			&product.CreatedAt,
			&product.UpdatedAt,
		); err != nil {
		c.log.Error("error is while updating category", logger.Error(err))
		return nil, err
	}
	return &product, nil
}

func (c *productRepo) Delete(ctx context.Context, key *pbC.PrimaryKey) (*empty.Empty, error) {
	query := `update products set deleted_at = extract(epoch from current_timestamp) where id = $1 `
	rowsAffected, err := c.db.Exec(ctx, query, key.GetId())
	if err != nil {
		c.log.Error("error is while deleting product", logger.Error(err))
		return nil, err
	}
	if row := rowsAffected.RowsAffected(); err != nil {
		c.log.Error("rows affected", logger.Any("rows", row))
	}
	return nil, err
}
