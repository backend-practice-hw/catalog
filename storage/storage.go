package storage

import (
	pbC "catalog/genproto/catalog_service"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
)

type IStorage interface {
	Close()
	Category() ICategoryStorage
	Product() IProductStorage
}

type ICategoryStorage interface {
	Create(context.Context, *pbC.CreateCategory) (*pbC.Category, error)
	Get(context.Context, *pbC.PrimaryKey) (*pbC.Category, error)
	GetList(context.Context, *pbC.CategoryRequest) (*pbC.CategoryResponse, error)
	Update(context.Context, *pbC.Category) (*pbC.Category, error)
	Delete(context.Context, *pbC.PrimaryKey) (*empty.Empty, error)
}

type IProductStorage interface {
	Create(context.Context, *pbC.CreateProduct) (*pbC.Product, error)
	Get(context.Context, *pbC.PrimaryKey) (*pbC.Product, error)
	GetList(context.Context, *pbC.GetProductRequest) (*pbC.GetProductResponse, error)
	Update(context.Context, *pbC.Product) (*pbC.Product, error)
	Delete(context.Context, *pbC.PrimaryKey) (*empty.Empty, error)
}
